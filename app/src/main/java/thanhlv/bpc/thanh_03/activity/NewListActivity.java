package thanhlv.bpc.thanh_03.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import thanhlv.bpc.thanh_03.R;
import thanhlv.bpc.thanh_03.adapter.NewsAdapter;
import thanhlv.bpc.thanh_03.interfaces.OnClick;
import thanhlv.bpc.thanh_03.model.Item;
import thanhlv.bpc.thanh_03.network.APIManager;

public class NewListActivity extends AppCompatActivity {

    RecyclerView rvListNews;
    List<Item> listData;
    NewsAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_list);
        //1
        getListData();
        //2
        listData = new ArrayList<>();
        adapter = new NewsAdapter(NewListActivity.this,listData);
        //3
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        //4
        rvListNews=findViewById(R.id.rvListNews);
        rvListNews.setLayoutManager(layoutManager);
        rvListNews.setAdapter(adapter);
        adapter.setOnClicks(new OnClick() {
            @Override
            public void onClickItem(int position)
            {
                Item model = listData.get(position);
                Intent intent = new Intent(NewListActivity.this, DetailActivity.class);
                intent.putExtra("URL",model.getContent().getUrl());
                startActivity(intent);
            }
        });


    }
    private void  getListData()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIManager service =retrofit.create((APIManager.class));
        service.getListData().enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if(response.body()!=null)
                {
                    listData = response.body();
                    adapter.reloadD(listData);
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Toast.makeText(NewListActivity.this,"False",Toast.LENGTH_LONG).show();
            }
        });
    }
}
