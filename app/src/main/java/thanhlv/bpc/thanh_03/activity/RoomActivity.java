package thanhlv.bpc.thanh_03.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import thanhlv.bpc.thanh_03.R;
import thanhlv.bpc.thanh_03.database.BookEntity;
import thanhlv.bpc.thanh_03.database.DbApp;

public class RoomActivity extends AppCompatActivity {
    DbApp db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        db = DbApp.getAppDatabase(this);

        insertBook();
        //updateBook(2);
        //getAllBook();
        //findBook(2);
    }
    private  void insertBook()
    {
        BookEntity bm = new BookEntity();
        bm.title = "zing";
        bm.content = "DYEWE";
        bm.date = "11-11-2010";
        //long id = db.bookdao().insertBookmark(bm);
        db.bookdao().insertBookmark(bm);
        //Toast.makeText(this,"ID: "+id,Toast.LENGTH_SHORT).show();
    }
    private  void updateBook(int id)
    {
        BookEntity bm = db.bookdao().getBookmark(id);
        bm.title="1213124";
        db.bookdao().updateBookmark(bm);
    }
    /*
    private  void getAllBook()
    {}
    private  void findBook(int id)
    {}
    */
}