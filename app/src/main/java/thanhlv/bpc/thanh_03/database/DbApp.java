package thanhlv.bpc.thanh_03.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {BookEntity.class},version = 1)
public abstract class DbApp extends RoomDatabase {

    private  static DbApp appDatabase;
    public  abstract  BookDao bookdao();
    public  static DbApp getAppDatabase(Context context)
    {
        if (appDatabase==null)
        {
            appDatabase = Room.databaseBuilder(context,DbApp.class,"News").allowMainThreadQueries().build();
        }
        return appDatabase;
    }



}
