package thanhlv.bpc.thanh_03.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface BookDao {

    @Insert(onConflict = REPLACE)
    void insertBookmark(BookEntity bookmark);
    @Update
    void  updateBookmark(BookEntity bookmark);
    @Delete
    void deleteBookmark(BookEntity bookmark);

    @Query("SELECT * FROM Book")
    List<BookEntity> getAllBookmark();

    @Query("SELECT * FROM Book WHERE id = :id")
    BookEntity getBookmark(int id);

    @Query("DELETE FROM Book")
    void deleteALL();
}
