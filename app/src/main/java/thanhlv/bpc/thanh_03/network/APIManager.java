package thanhlv.bpc.thanh_03.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import thanhlv.bpc.thanh_03.model.Item;

public interface APIManager {
    String url="https://api-demo-anhth.herokuapp.com";

    @GET("data.json")
    Call<Item> getItemData();

    @GET("datas.json")
    Call<List<Item>> getListData();

}
